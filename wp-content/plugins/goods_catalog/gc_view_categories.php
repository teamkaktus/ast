<?php
global $wpdb;

if (function_exists('wp_enqueue_media')) {
    wp_enqueue_media();
}

?>
<style>
    table img{
        width:  64px;
        height: 64px;
    }

</style>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<h3>Категории товаров</h3>
<div id="add_cat_container">
    <form id="add_cat_f" class="form-horizontal" enctype="multipart/form-data">

        <table class="table">
            <tr>
                <th>Название</th>
                <th><input type="button" class="btn btn-default" id="cat_thumb_b" name="cat_thumb" value="Задать миниатюру"></th>
                <th><input type="button" class="btn btn-default" id="cat_img_b" name="cat_img" value="Задать картинку"></th>
                <th></th>
            </tr>
            <tr>
                <td><input type="text" class="form-control" id="cat_name" name="cat_name"></td>
                <td><input type="text" id="cat_thumb_url" name="cat_thumb_url" hidden><img id="cat_thumb" src=" "></td>
                <td><input type="text" id="cat_img_url" name="cat_img_url" hidden><img id="cat_img" src=" "></td>
                <td> <input type="button" class="btn btn-default" id="add_cat" value="Добавить"></td>
            </tr>
        </table>
    </form>

</div>
<div id="cat_container">
    <h3>Существующие категории</h3>
    <table class="table">
        <tr><th>Изображение</th><th>Миниатюра</th><th>Название</th><th class="text-center" colspan="2">Удалить(если пустая)/Изменить</th></tr>
        <?php
        $cat = $wpdb->get_results( "SELECT *  FROM ".$wpdb->prefix."gc_categories", OBJECT_K);

        foreach ($cat as $row){
            $view_cat .= "<tr><td><img class='upd_cat_img' id='img_upd_cat_img".$row->id."' src=\"http://".$_SERVER['SERVER_NAME'].$row->image."\">
                                  <input id='inp_upd_cat_img".$row->id."' value='".$row->image."' hidden></td>

                              <td><img class='upd_cat_thumb' id='img_upd_cat_thumb".$row->id."' src=\"http://".$_SERVER['SERVER_NAME'].$row->thumbnail."\">
                                  <input id='inp_upd_cat_thumb".$row->id."' value='".$row->thumbnail."' hidden></td>";

            $view_cat .= "<td><input type='text' class=\"form-control\" name='upd_cat_name".$row->id."' value='".$row->name."' ></td>";

            $query = "SELECT Count(categories.id) AS id_count
                        FROM
                          ".$wpdb->prefix."gc_categories AS categories ,
                          ".$wpdb->prefix."gc_products AS products
                        WHERE
                          products.cat_id = categories.id AND
                          categories.id = ".$row->id;

            $child_exist = $wpdb->get_results($query);
            if($child_exist[0]->id_count == 0 && $row->id > 0){
                $view_cat .= "<td><input type=\"button\" value=\"Удалить\" class=\"del_cat btn btn-default\" id=\"del_cat".$row->id."\">";
            }else{
                $view_cat .= "<td><input type=\"button\" class='btn btn-default' value=\"Удалить\"  disabled></td>";
            }
            $view_cat .= "<td><input type='button' value='Обновить' class='upd_cat btn btn-default' id='upd_cat".$row->id."'></td></tr>";
        }

        echo $view_cat;
        ?>

    </table>
</div>
