<footer class="footer">
    <div class="col-sm-12">
        <div class="col-sm-2">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_logo.png">
        </div>
        <div class="col-sm-10">
            <div class="col-sm-2" style="padding: 0 3px;">
                <div class="col-sm-12" style="padding:0px; text-align:center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_time_icon.png">
                </div>
                <div class="col-sm-12 footer_icon_text"  style="padding:0px">
                     <span>График работы:</span><br>
                    <?php print_r(get_option('theme_worktext')); ?>
                </div>
            </div>
            <div class="col-sm-2" style="padding: 0 3px;">
                <div class="col-sm-12" style="padding:0px; text-align:center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_marker_icon.png">
                </div>
                <div class="col-sm-12 footer_icon_text"  style="padding:0px">
                    <span>Адрес:</span><br>
                    <?php print_r(get_option('theme_address')); ?>
                </div>
            </div>
            <div class="col-sm-2" style="padding: 0 3px;">
                <div class="col-sm-12" style="padding:0px; text-align:center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_telefon_icon.png">
                </div>
                <div class="col-sm-12 footer_icon_text"  style="padding:0px">
                    <span>Телефон:</span><br>
                    <?php print_r(get_option('theme_telephone')); ?>
                </div>
            </div>
            <div class="col-sm-2" style="padding: 0 0px;">
                <div class="col-sm-12" style="padding:0px; text-align:center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/footer_mail_icon.png">
                </div>
                <div class="col-sm-12 footer_icon_text" style="padding:0px;">
                    <span>Email:</span><br>
                    <?php print_r(get_option('theme_email')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12 social_icons">
                    <div class="col-sm-2 pull-right" style="padding:3px;"><a href="<?php print_r(get_option('theme_soc_ok')); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social/ok.png" alt="vk" style="width:100%;"></a></div>
                    <div class="col-sm-2 pull-right" style="padding:3px;"><a href="<?php print_r(get_option('theme_soc_f')); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social/f.png" alt="vk" style="width:100%;"></a></div>
                    <div class="col-sm-2 pull-right" style="padding:3px;"><a href="<?php print_r(get_option('theme_soc_m')); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social/m.png" alt="vk" style="width:100%;"></a></div>
                    <div class="col-sm-2 pull-right" style="padding:3px;"><a href="<?php print_r(get_option('theme_soc_t')); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social/t.png" alt="vk" style="width:100%;"></a></div>
                    <div class="col-sm-2 pull-right" style="padding:3px;"><a href="<?php print_r(get_option('theme_soc_vk')); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social/vk.png" alt="vk" style="width:100%;"></a></div>
                </div>
                <div class="col-sm-12 social_icons">
                    <p style="color:white; font-size: 9px; font-family: 'Exo 2'; margin-top: 10px;" class="pull-right">© 2016. Все права защищены</p>
                    <img class="pull-right" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" style="width:90%">
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
