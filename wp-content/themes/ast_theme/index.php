<?php
/*
Template Name: Шаблон главной
*/
get_header(); ?>

<?php $categories = get_all_cats($wpdb); ?>

<div class="main-wrap">
    <div class="grey_fon">
        <div class="slogan_text">
            <div class="orenda">АРЕНДА</div>
            <div class="orenda2">СТРОИТЕЛЬНОЙ ТЕХНИКИ</div>
            <div class="orenda3">
                <ul class="oput" style="padding-left: 0px">
                    <li style="display: inline;">ОПЫТ</li>
                    <li style="display: inline;">СЕРВИС</li>
                    <li style="display: inline;">КАЧЕСТВО</li>
                </ul>
            </div>
        </div>
        <div class="work-text-container">
            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/golgov_foto.png">
            <span>Звонить <?php print_r(get_option('theme_worktext')); ?></span>
        </div>
    </div>
    <div class="col-sm-12">
        <?php
        foreach ($categories as $category) { ?>
            <div class="col-sm-2" style="text-align: center">
                <a href="<?=get_home_url();?>/categories?id=<?= $category->id; ?>">
                    <img src="<?=get_home_url();?>.<?= $category->thumbnail; ?>" alt="" style="width: 100%">
                    <div class="categ_name1"><?= $category->name; ?></div>
                </a>
            </div>
        <?php } ?>
    </div>
    <div class="col-sm-12"
         style="border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;padding-top: 10px;padding-bottom: 10px;margin-bottom: 10px;margin-top: 20px">
        <nav>
            <?php
            wp_nav_menu(array(
                'menu_class' => '',
                'theme_location' => 'main',
                'after' => ''
            ));
            ?>
        </nav>
    </div>
    <div class="col-sm-12" style="margin-bottom: 25px;">
        <?php

        $i = 0;
        foreach ($categories as $category) {
            if ($i == 3) {
                break;
            }
            ?>
            <div class="col-sm-4 homeCategoryHover" style="position:relative;padding-left:2px;padding-right: 2px">
                <a href="/categories?id=<?= $category->id; ?>">
                    <img src=".<?= $category->image; ?>" alt="" style="width:100%;">
                    <div class="col-sm-12" style="padding:0px;position:relative;">
                        <div class="absolute_fon" style="padding-left:20px;">
                            <div class="categoryName"><?= $category->name; ?> <img
                                    src="<?php echo esc_url(get_template_directory_uri()); ?>/images/viewIcon.png"
                                    class="viewIcon"></div>
                            <span class="category-after-name">Всего</span>
                            <span class="countProductCategory"><?= $category->p_count ? $category->p_count : 0; ?></span>
                            <span class="category-after-name">моделей</span>
                        </div>
                    </div>
                </a>
            </div>
            <?php $i++;
        }
        ?>
    </div>
    <section class="main-content">
        <div class="col-sm-12 styleTextPages">
            <?php
            if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h1><?php the_title(); ?></h1>
                <?php the_content() ?>
                <?php
            endwhile;
            else:
                _e('Извините такой страницы не найдено!');
            endif;
            ?>
        </div>
    </section>
    <div style="clear:both;"></div>
</div>
<?php get_footer(); ?>
