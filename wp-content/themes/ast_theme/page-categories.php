<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="<?php echo get_post_meta($post->ID, 'description', true); ?>" />
    <meta name="keywords" content="<?php echo get_post_meta($post->ID, 'keywords', true); ?>" />
    <link rel="shortcut icon" href="/favocin.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=geometry'></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <title><?php $category = get_category_name($wpdb, $_GET['id']);
        echo $category[0]->name; ?></title>
    <?php wp_head() ?>
</head>
<body>
<div class="containter" style="height: 136px;">
    <div class="header_fon">
        <div class="text-color_grey map-marker">
            <?php print_r(get_option('theme_address')); ?>
        </div>
        <div class="text-color_grey email-block">
            <?php print_r(get_option('theme_email')); ?>
        </div>
        <div class="call_back">
            <a data-toggle="modal" data-target="#myModal" class="btn btn-default pull-right call_back_button">Заказать звонок</a>
        </div>
        <div class="text-color_grey phone-block">
            <?php print_r(get_option('theme_telephone')); ?>
        </div>
    </div>
    <div class="ast_logo_block">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ast_logo.png">
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog" style="margin-top: 200px;" >
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="background-color: #f2f2f2;width:300px;margin-left: 150px">
        <div class="modal-header" style="text-align: center">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" style='padding:0px;margin-top:30px;margin-bottom: 30px' align='center'>ЗАКАЗАТЬ ЗВОНОК</h3>
          <form>
              <input type="hidden" name="type" value="call_back">
              <input type="text" class="form-control" placeholder="Ваше имя:" name="name" style="width:90%;margin:0 auto; margin-bottom:30px;"  required>
              <input type="text" class="form-control"  placeholder="Ваш номер телефона:"  name="telefon"  style="width:90%;margin:0 auto;margin-bottom:30px"  required>
              <input type="submit" class="btn btn-default call_back_button" style="color:white;margin:0 auto;font-size: 16px" value="Заказать">
          </form>
        </div>
      </div>

    </div>
  </div>
    
    <?php $categories = get_all_cats($wpdb); ?>
     <div class="col-sm-12 grey_fon" style="height:105px;margin-bottom: 40px">
        <div class="col-sm-12">
            <h1 class="headTitle">
                <b>
                    <?php
                    $category = get_category_name($wpdb, $_GET['id']);
                    echo $category[0]->name;
                    ?>
                </b>
            </h1>
        </div>
    </div>

    <div class="col-sm-12">
        <?php
        foreach ($categories as $category) { ?>
            <div class="col-sm-2" style="text-align: center">
                <a href="<?=get_home_url();?>/categories?id=<?= $category->id; ?>">
                    <img src="<?=get_home_url();?>.<?= $category->thumbnail; ?>" alt="" style="width: 100%">
                    <div class="categ_name1"><?= $category->name; ?></div>
                </a>
            </div>
        <?php } ?>
    </div>
    <div class="col-sm-12"
         style="border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;padding-top: 10px;padding-bottom: 10px;margin-bottom: 10px;margin-top: 20px">
        <nav>
            <?php
            wp_nav_menu(array(
                'menu_class' => '',
                'theme_location' => 'main',
                'after' => ''
            ));
            ?>
        </nav>
    </div>
    <?php

    $products = get_product_by_cat($_GET['id'], $wpdb);
    foreach ($products as $product) { ?>
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <div class="categ-product-head"><div class="text-head"><a href="<?= get_home_url(); ?>/product/?id=<?= $product->id; ?>"><?=$product->name;?></a></div></div>
                </div>
                <div class="col-sm-3">
                    <?php
                    if(isset($product->images)) {
                        foreach ($product->images as $image) { ?>
                            <a href="<?= get_home_url(); ?>/product/?id=<?= $product->id; ?>">
                                <img src="<?= get_home_url(); ?>/<?= $image->url; ?>" alt="<?= $product->name; ?>"
                                     style="width:100%;border:1px solid #d9d9d9;">
                            </a>
                        <?php }

                    } ?>
                </div>
                <div class="col-sm-9">
                    <div class="col-sm-8">
                        <?php if(isset($product->params)){ ?>
                        <table class="table table-bordered product_options">
                            <tr>
                                <th style="background-color: #f2f2f2" colspan="3">Основные технические характеристики</th>
                            </tr>
                            <?php foreach($product->params as $param){ ?>
                                <tr>
                                    <td><?=$param->name;?></td>
                                    <td><?=$param->unit;?></td>
                                    <td><?=$param->value;?></td>
                                </tr>    
                            <?php } ?>
                        </table>
                        <?php } ?>
                        <div class="row">
                            <a href="<?=get_home_url();?>/product?id=<?=$product->id;?>">
                                <button class="btn btn-default pull-right more-button"><b>Подробнее</b></button>
                            </a>
                        </div>
                     </div>
                    <?php if(isset($product->prices)){ ?>
                        <div class="col-sm-4">
                            <table class="table tableOrenda table-bordered">
                                <tr>
                                    <th style="background-color: #b3b3b3;color:white;">Аренда</th>
                                    <th style="background-color: #b3b3b3;color:white;">&#8381;</th>
                                </tr>
                                <?php foreach($product->prices as $price){ ?>
                                    <tr>
                                        <td style="background-color: #f2f2f2;color:#808080;"><?=$price->name;?></td>
                                        <td style="background-color: #f2f2f2;color:#95b001;"><?=$price->value;?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                            <div class="row">
                                <button data-toggle="modal" data-target="#myModal<?=$product->id;?>"   class="btn btn-default pull-right rent-button" data-name="<?=$product->name;?>"><b>Арендовать</b></button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal<?=$product->id;?>" role="dialog" style="margin-top: 200px;" >
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content" style="background-color: #f2f2f2;width:300px;margin-left: 150px">
                <div class="modal-header" style="text-align: center">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h3 class="modal-title" style='padding:0px;margin-top:30px;margin-bottom: 20px' align='center'>ЗАПОЛНИТЬ ФОРМУ</h3>
                  <div style="padding-left: 20px">
                    <span style="float:left;">Арендовать:</span><br>
                    <h4 style="color:#ffd200;text-align:left"><?=$product->name;?></h4>
                  </div>
                  <form>
                      <input type="hidden" name="type" value="orenda">
                      <input type="hidden" name="tovar" value="<?=$product->name;?>">
                      <input type="text" class="form-control" placeholder="Ваше имя:" name="name" style="width:90%;margin:0 auto; margin-bottom:30px;" required>
                      <input type="text" class="form-control"  placeholder="Ваш номер телефона:"  name="telefon"  style="width:90%;margin:0 auto;margin-bottom:30px" required>
                      <textarea class="form-control" placeholder="Примечания" name="description"  style="width:90%;margin:0 auto;margin-bottom:30px"></textarea>
                      <input type="submit" class="btn btn-default" style="background-color: #95b001;color:white;margin:0 auto;font-size: 20px" value="Заказать">
                  </form>
                </div>
              </div>

            </div>
        </div>
    <?php } ?>

    <div class="main-wrap">
        <section class="main-content">
            <div class="col-sm-12 styleTextPages ">
                <?php
                if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <h2><?php the_title(); ?></h2>
                    <div class="col-sm-12 textContent">
                    <?php
                        the_content();
                        endwhile;
                        else:
                            _e('Извините такой страницы не найдено!');
                        endif;
                        ?>
                    </div>
            </div>
    </section>
    </div>
    <div style="clear: both"></div>
<?php get_footer(); ?>