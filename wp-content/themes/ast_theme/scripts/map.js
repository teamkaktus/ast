/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 55.7498598, lng: 37.3523182},
        zoom: 10
    });
    var geocoder = new google.maps.Geocoder();
    var address = document.getElementById('address').value;
    
    geocoder.geocode({'address': address}, function(results, status) {
        var Local = results[0].geometry.location;
        var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
        });
    })
}

$(document).ready(function(){
    initMap();
});
